/*Every Angular application has at least one component: the root component, named AppComponent here.*/

import { Component } from '@angular/core';
import { Events } from './events.component';
import { TracksComponent } from './tracks/tracks.component';


@Component({
    selector: 'my-app',
    templateUrl: 'app/app.component.html',
    styleUrls: ['app/app.component.css']
})

/*You export the AppComponent class so that you can import it into the application that you created.*/
export class AppComponent {



}
