import { Component } from '@angular/core';
// import { Event } from './model/events';
// import { EventsDataService } from "./events-data.service";
// import { TracksComponent } from "./tracks.component";

//defines a class and the properties it contains...must be before component decorator
export class Races{
    race: string;
    grid: number;
    result: number;
    fastlap: string;
}

export class Events{
    track: string;
    date: string;
    races: Races[];
}

export class Racers{
    num: string;
    name: string;
    category: string;
    data: Events[];
}

//sample data in JSON format
const RACERS: Racers[] = [{
    "num": "108",
    "name": "Marcus Edwards",
    "category": "Expert",
    "data": [
        {
            "track": "Summit Point", "date": "4-17-15", "races": [
            {
                "race": "mwss",
                "grid": 24,
                "result": 5,
                "fastlap": "1:20.9"},
            {
                "race": "hwss",
                "grid": 30,
                "result": 12,
                "fastlap": "1:18.9"
            }]
        },
        {
            "track": "NJMP", "date": "4-12-15", "races": [
            {
                "race": "mwsb",
                "grid": 30,
                "result": 2,
                "fastlap": "1:19.9"},
            {
                "race": "ulss",
                "grid": 13,
                "result": 12,
                "fastlap": "1:22.9"
            }]
        }] //end data array
}];


//********** COMPONENT ************

@Component({
    selector: 'app-events',
    templateUrl: 'app/events.component.html',
    styleUrls:['app/events.component.css']
})


//class for data binding
export class EventsComponent {

    //class resources
    racers = RACERS;   //public property that exposes the RACERS array, of type Racer
    private searchValue; //local value
    public showResults = false;

    constructor(){}

    // (3) takes the string value and assigns it to this object searchValue property
    searchRacer(value: string): void {
      this.searchValue = value;
      this.showResults = true;
    }

    generateArray(obj){
        return Object.keys(obj).map((key)=>{ return obj[key]});
    }


    //initialize the data layer
    ngOnInit(){
        //this.eventsService.getEvents().subscribe(events => this.events = events);
    }

}








