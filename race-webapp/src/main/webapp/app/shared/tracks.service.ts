import { Track } from './tracks.model';

export class TracksService {

    private TRACKS: Track[] = [
        {name: 'New Jersey Motorsports Park', distance: 2.25, turns: 12,region: 'ATL, MA', asra: false },
        {name: 'Summit Point', distance: 2.0, turns: 10, region: 'MA', asra: true }
    ];

    getTracks(){
        return this.TRACKS;
    }

}