

export class Track{

    public name: string;
    public distance: number;
    public turns: number;
    public region: string;
    public asra: boolean;

    constructor(name: string, distance: number, turns: number, region: string, asra: boolean) {
        this.name = name;
        this.distance = distance;
        this.region = region;
        this.turns = turns;
        this.asra = asra;
    }


}