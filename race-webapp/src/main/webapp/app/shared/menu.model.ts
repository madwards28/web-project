


export class Menu{

    public id: number;
    public page: string;
    public route: string

    //initialize an object of type Menu
    constructor(id: number, page: string, route: string){
        this.id = id;
        this.page = page;
        this.route = route;
    }

}
