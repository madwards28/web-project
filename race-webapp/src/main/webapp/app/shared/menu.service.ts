import { Menu } from "./menu.model";

export class MenuService{

    /*Use these private properties to instantiate and set different menu items. Probably not necessary for more than
    the main menu*/


    //generic array using the menu model
    private MAIN: Menu[] = [
        {id: 0, page: 'Home', route: ''},
        {id: 1, page: 'About', route: '/about'},
        {id: 2, page: 'Events', route: '/events'},
        {id: 3, page: 'Tracks', route: '/tracks'},
        {id: 4, page: 'Media', route: '/media'},
        {id: 5, page: 'Contact', route: 'contact'}
    ];

    getMainMenu(){
        return this.MAIN;
    }



}
