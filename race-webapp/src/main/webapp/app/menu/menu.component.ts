import { Component, OnInit } from '@angular/core';
import { MenuService } from '../shared/menu.service';


@Component({
    selector: 'menu',
    templateUrl: 'app/menu/menu.component.html',
    styleUrls:['app/menu/menu.component.css']
})

export class MenuComponent {

    nav = [];



    /* **COMPONENT RESOURCES ***/

    //creates the MenuService instance when component is created
    constructor(private MenuService: MenuService){}

    //gets menu from service and assigns it to this component's navs property
    ngOnInit(){
        this.nav = this.MenuService.getMainMenu();
    }
}




