import {EVENTS} from './model/mocks';
import {Injectable} from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()

export class EventsDataService{

    constructor(private http: Http){}

    getEvents(){
        return this.http.get('app/model/events.json').map(response => <Event[]>response.json().racer);
    }

}