import {Event} from './events';


//noinspection TypeScriptValidateTypes
export const EVENTS: Event[] =  [{ //Tells Typescript to treat this like an array of Events
    "track": "New Jersey Motorsports Park",
    "date": new Date('2015-01-04'),
    "race": "hwss",
    "place": 4,
        "grid": 10,
        "points": 12,
        "fastlap": "1:20.94"
    }, {
    "track": "Summit Point Motorsports Park",
    "date": new Date('2015-04-22'),
    "race": "mwss",
    "place": 5,
        "grid": 10,
        "points": 15,
        "fastlap": "1:45.24"
}];