/**
 * MODEL DATA ARRAY
 */
export class Event {
    track: string;
    date: Date;
    race: string;
    place: number;
    grid: number;
    points: number;
    fastlap: string;
}