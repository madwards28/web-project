import { Component, OnInit } from '@angular/core';
import { TracksService } from '../shared/tracks.service';

@Component({
    selector: 'app-tracks',
    templateUrl: 'app/tracks/tracks.component.html',
    styleUrls: ['app/tracks/tracks.component.css']
})


export class TracksComponent {
    trackList = [];
    opened: Boolean =false;

    constructor (private TracksService: TracksService) {}

    ngOnInit(){
      this.trackList = this.TracksService.getTracks();
    }
}

