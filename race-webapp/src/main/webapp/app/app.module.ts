/*This is entry point into the app. Make declarations for all additional components here*/

/*Modules*/
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';


/*Components*/
import { AppComponent }   from './app.component';
import { EventsComponent } from "./events.component";
import { MenuComponent } from "./menu/menu.component";
import { TracksComponent } from "./tracks/tracks.component";
import { MediaComponent } from "./media/media.component";

/*Services*/
import { EventsDataService } from './events-data.service';
import { MenuService } from "./shared/menu.service";
import { TracksService } from "./shared/tracks.service";



/*Page Routing*/
const appRoutes: Routes = [
    {
        path: 'events',
        component: EventsComponent,
        data: {
            title: 'Event List'
        }
    },
    {path: 'media', component: MediaComponent},
    {path: 'tracks', component: TracksComponent}

    // { path: '', component: HomeComponent },
    // { path: '**', component: PageNotFoundComponent }
];



@NgModule({
    imports:      [ BrowserModule, FormsModule, HttpModule, RouterModule.forRoot(appRoutes)],
    declarations: [ AppComponent, EventsComponent, MenuComponent, TracksComponent, MediaComponent],
    bootstrap:    [ AppComponent ],
    providers:    [ EventsDataService, MenuService, TracksService ]

})
export class AppModule { }
