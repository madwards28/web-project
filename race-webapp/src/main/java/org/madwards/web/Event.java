package org.madwards.web;

import java.time.*;

public class Event {
    private String date;
    private Track track;
    private boolean dblPoints;
    private boolean twinSprints;

    public Event(String date, Track track, boolean dblPoints, boolean twinSprints) {
        this.date = date;
        this.track = track;
        this.dblPoints = dblPoints;
        this.twinSprints = twinSprints;
    }

    @Override
    public String toString() {
        String toString = "\tEVENT: " +
                "\n\t\tDate:" + date
                + "\n\t\t" + track;
        return toString;
    }

    public String getDate() {
        return date;
    }

    public String getTrack() {

        return track.toString();
    }

    public boolean isDblPoints() {
        return dblPoints;
    }

    public boolean isTwinSprints() {
        return twinSprints;
    }
}
