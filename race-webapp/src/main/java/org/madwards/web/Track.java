package org.madwards.web;

/**
 * Created by Madwards on 12/4/2016.
 */
public class Track {

    private String track;
    private String state;
    private String city;
    private String zip;
    private double length;
    private int turns;



    public Track(String track, String state, String city, String zip, double length, int turns) {
        this.track = track;
        this.state = state;
        this.city = city;
        this.zip = zip;
        this.length = length;
        this.turns = turns;
    }

    @Override
    public String toString() {

//        String toString = track
//                + ", " + address;
        return track;
    }

    public String getTrack() {
        return track;
    }
}
