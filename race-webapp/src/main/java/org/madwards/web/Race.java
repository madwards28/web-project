package org.madwards.web;

//import java.time.LocalDate;
//import java.util.Objects;
import java.util.Arrays;


public class Race extends Event {

    private String raceClass;
    private Rider[] riders;

    //Needs a sorted list of results
    public Race(String date, Track track, boolean dblPoints, boolean twinSprints, String raceClass, Rider[] riders) {
        super(date, track, dblPoints, twinSprints);
        this.raceClass = raceClass;
        this.riders = riders;
    }

    @Override
    public String toString() {
        String toString = "/****Race***/\n" +
                "\tCLASS: " + raceClass +
                "\n\tRIDERS: " + Arrays.toString(riders) +
                "\n" + super.toString();
        return toString;
    }

    /****** Getters and Setters *********/

    public Rider[] getRiders() {
        return riders;
    }

    public void setRiders(Rider[] riders) {
        this.riders = riders;
    }

    public String getRaceClass() {
        return raceClass;
    }
}
