package org.madwards.web;

/*
* Object created for each race...not global objects. For global profile create a RiderResume class
* create methods to set lap times and results
* create array of laps for each rider/event
 */

import javax.json.Json;
import javax.json.JsonObject;
import java.util.Arrays;

public class Rider {

    private String name;
    private int number;
    private String transponder;
    private String category;
//    private String[] lapTime;
//    private Race[] racesCompeted;

    /****** Constructor ***********/

    public Rider(){}

    public Rider(String name, int number, String transponder, String category) {
        this.name = name;
        this.number = number;
        this.transponder = transponder;
        this.category = category;
//        this.racesCompeted = racesCompeted;
    }

    /****** Getters and Setters *********/

    public String getName() {
        return name;
    }

    public int getNumber() {
        return number;
    }

    public String getCategory() {
        return category;
    }

    @Override
    public String toString() {
        String toString = name + "/" + number + "/" + category;

        return toString;
    }

//    public JsonObject createRiderJson (){
//        JsonObject model = Json.createObjectBuilder()
//                .add("name", this.getName())
//                .add("number", this.getNumber())
//                .add("category", this.getCategory())
//                .build();
//        return model;
//    }

    public String getTransponder() {
        return transponder;
    }
}
