package org.madwards.test;

/* Uses javax.json 1.0 external library. Needs jars for both compile and runtime (separate jars)
*   See JSR 353 for usage
* */

import org.madwards.web.*;
import org.elasticsearch.*;

import java.io.*;
import javax.json.*;

public class DataTest {

    public static void main(String[] args) throws IOException {

        /****** STATIC DATA TO TEST OBJECT MODELS ******/

        //Track Model
        Track summit = new Track("Summit Point Motorsports Park", "WV", "Summit Point", "25446", 2.0,  10);

        //Event Model
        Event event1 = new Event("April 12, 2015", summit, false, false);

        //Rider Model
        Rider[] riderArray = new Rider[3];
        riderArray[0] = new Rider("Marcus Edwards", 108, "12345",  "AM");
        riderArray[1] = new Rider("Korey Hopkins", 469, "465893", "AM");
        riderArray[2] = new Rider("D-bag McLoserson", 666, "88567", "AM");

        //Race Model
        Race race1 = new Race("April 12, 2015", summit,false, false, "MWSS", riderArray );

        /******* TEST OUTPUT *********/

        //Basic toStrings
//        System.out.println(event1);
//        System.out.println(riderArray[0]);
//        System.out.println();
//        System.out.println(race1);
//        System.out.println();
//        System.out.println(summit);

        //PrintStream and its println method will add OS dependent line separator at the end of the string automatically
        //otherwise simply use Filewriter
        try(PrintStream ps = new PrintStream(new File("rider_model.txt"))) {
            for (Rider r : riderArray) {
//                System.out.println(r.toString()); //prints toString of Objects
                ps.println(createRiderJson(r)); //prints json strings on separate lines to file
            }
        }

        try(PrintStream ps = new PrintStream(new File("race_model.txt"))) {
                ps.println(createRaceJson(race1)); //prints json strings on separate lines to file
            }

    }//end main

    private static JsonObject createRiderJson (Rider r){
        JsonObject model = Json.createObjectBuilder()
                .add("name", r.getName())
                .add("number", r.getNumber())
                .add("transponder", r.getTransponder())
                .add("category", r.getCategory())
                .build();
        return model;
    }

    private static JsonObject createRaceJson (Race r){
        JsonObject model = Json.createObjectBuilder()
                .add("date", r.getDate())
                .add("track", r.getTrack())
                .add("Double", r.isDblPoints())
                .add("Twin", r.isTwinSprints())
                .add("category", r.getRaceClass())
                .build();
        return model;
    }
}
